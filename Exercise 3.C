#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define MAX 1000 /* The maximum number of characters in a line of input */

int main()
{
  char text[MAX], c;
  puts("Type some text (then ENTER):");
  fgets(text, MAX, stdin);  
  printf("Your input in reverse is:\n");

  int length = strlen(text) - 1;
  char reverseText[length];
  
  for(int i = 0; i < length; i++) {
    reverseText[i] = text[(length - 1) - i];
  }
  
  reverseText[length] = '\0';
  printf("%s\n", reverseText);
  
  int textPosition = 0;
  char editedText[length];
  for (int i = 0; i < length; i++) {
      if (ispunct(text[i])) {
          continue;
      }
      if (isalpha(text[i])) {
          editedText[textPosition] = tolower(text[i]);
          textPosition++;
      }
  }
  editedText[textPosition] = '\0';
  // printf("%s\n", editedText);
  
  
  int textPosition2 = 0;
  char editedReverseText[length];
  for (int i = 0; i < length; i++) {
      if (ispunct(reverseText[i])) {
          continue;
      }
      if (isalpha(reverseText[i])) {
          editedReverseText[textPosition2] = tolower(reverseText[i]);
          textPosition2++;
      }
  }
  editedReverseText[textPosition2] = '\0';
  // printf("%s\n", editedReverseText);
  
  

  int palindrome = 1;
  for (int i = 0; i < textPosition2; i++) {
    if (editedText[i] != editedReverseText[i]) {
        palindrome = 0;
    }
  }
    if (palindrome == 1) {
      printf("Found a palindrome!\n");
  }
 

}
